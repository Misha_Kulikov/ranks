public class Ranks {
    public static void main(String[] args) {

        int numberOne = 5; //Разряды числа: 101
        intToBits(numberOne);

        int numberTwo = -5; //Разряды числа: 11111111111111111111111111111011
        intToBits(numberTwo);

        double realNumberOne = -2.5; //Формат чисел с плавающей точкой: 1100000000000100000000000000000000000000000000000000000000000000
        doubleToBits(realNumberOne);

        double realNumberTwo = 2.5; //Формат чисел с плавающей точкой: 0100000000000100000000000000000000000000000000000000000000000000
        doubleToBits(realNumberTwo);
    }

    private static void doubleToBits(double number) {
        String sResult;
        long numberBits = Double.doubleToLongBits(number);

        sResult = Long.toBinaryString(numberBits);
        System.out.println("Представление вещественного числа в формате чисел с плавающей точкой");

        System.out.format("Число: %5.2f\n", number);
        System.out.println("Формат чисел с плавающей точкой:");
        System.out.println(number > 0 ? "0" + sResult : sResult);
    }

    private static void intToBits(int number) {
        String intBits = Integer.toBinaryString(number);
        System.out.format("Число: %d%n", number);
        System.out.println("Разряды числа: " + intBits);
    }
}
